const router = require('express').Router();
const trucksController = require('./../controllers/trucksController');

router.get('/', trucksController.getTrucks);
router.post('/', trucksController.addTruck);
router.get('/:id', trucksController.getTruck);
router.put('/:id', trucksController.updateTruck);
router.delete('/:id', trucksController.deleteTruck);
router.post('/:id/assign', trucksController.assignTruck);

module.exports = router;