const router = require('express').Router();
const authController = require('./../controllers/authController');
const { registrationValidator } = require('./../middelwares/validationMiddelware');

router.post('/register', registrationValidator, authController.register);
router.post('/login', authController.login);

module.exports = router;