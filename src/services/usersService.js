const User = require('./../models/userModel');
const Credentials = require('./../models/credentialsModel');
const RegistrationCredentials = require('./../models/registartionCredentialsModel');
const bcrypt = require('bcrypt');

const getUser = async (email) => {
    const user = await User.findOne({ email });
    return user;
}

const deleteUser = async (email) => {
    const user = await User.findOne({ email });
    if (user) {
        await User.findOneAndRemove({ email });
        await Credentials.findOneAndRemove({ email });
        await RegistrationCredentials.findOneAndRemove({ email });
    } else {
        throw new Error('User doesn\'t exist');
    }
}

const changePassword = async (email, oldPassword, newPassword) => {
    const oldCredentials = await Credentials.findOne({ email });
    if (!(await bcrypt.compare(oldPassword, oldCredentials.password))) {
        throw new Error('Invalid old password');
    } else {
        await Credentials.findOneAndUpdate({ email }, { password: await bcrypt.hash(newPassword, 10) });
        await RegistrationCredentials.findOneAndUpdate({ email }, { password: await bcrypt.hash(newPassword, 10) });
    }
}

module.exports = {
    getUser,
    deleteUser,
    changePassword
}