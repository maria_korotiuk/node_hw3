const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('./../models/userModel');
const Credentials = require('./../models/credentialsModel');
const RegistrationCredentials = require('./../models/registartionCredentialsModel');

const registration = async (email, password, role) => {
    const user1 = await User.findOne({ email });
    if (user1) {
        throw new Error ('Email has been already used');
    }
    const credentials = new Credentials({
        email,
        password: await bcrypt.hash(password, 10)
    });
    await credentials.save();

    const registrationCredentials = new RegistrationCredentials({
        email,
        password: await bcrypt.hash(password, 10),
        role
    });
    await registrationCredentials.save();

    const user = new User({
        role,
        email
    });
    await user.save();
}

const loginUser = async (email, password) => {
    const credentials = await Credentials.findOne({ email });
    if (!credentials) {
        throw new Error('Invalid email or password');
    }
    if (!(await bcrypt.compare(password, credentials.password))) {
        throw new Error('Invalid email or password');
    }
    const token = jwt.sign({
        _id: credentials._id,
        email: credentials.email
    }, 'secret');
    return token;
}

module.exports = {
    registration,
    loginUser
}