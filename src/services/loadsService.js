const Load = require('./../models/loadModel');
const User = require('./../models/userModel');
const Truck = require('./../models/truckModel');

const SPRINTER = {
    width: 300,
    length: 250,
    height: 170,
    payload: 1700
}

const SMALL_STRAIGHT = {
    width: 500,
    length: 250,
    height: 170,
    payload: 2500
}

const LARGE_STRAIGHT = {
    width: 700,
    length: 350,
    height: 200,
    payload: 4000
}

const findTruck = async (load) => {
    if (load.dimensions.width <= SPRINTER.width || load.dimensions.width <= SPRINTER.length) {
        if (load.dimensions.length <= SPRINTER.width || load.dimensions.length <= SPRINTER.length) {
            if (load.dimensions.heigth <= SPRINTER.height || load.dimensions.height <= SPRINTER.height) {
                if (load.payload <= SPRINTER.payload) 
                {
                    const truck = await Truck.findOne({ status: 'IS', type: 'SPRINTER' });
                    if (truck) {
                        if (truck.assigned_to) 
                        {
                            return truck;
                        }
                    }
                }
            }
        }
    } else if (load.dimensions.width <= SMALL_STRAIGHT.width || load.dimensions.width <= SMALL_STRAIGHT.length) {
        if (load.dimensions.length <= SMALL_STRAIGHT.width || load.dimensions.length <= SMALL_STRAIGHT.length) {
            if (load.dimensions.heigth <= SMALL_STRAIGHT.height || load.dimensions.height <= SMALL_STRAIGHT.height) {
                if (load.payload <= SMALL_STRAIGHT.payload) 
                {
                    const truck = await Truck.findOne({ status: 'IS', type: 'SMALL STRAIGHT' });
                    if (truck) {
                        if (truck.assigned_to) 
                        {
                            return truck;
                        }
                    }
                }
            }
        }
    } else if (load.dimensions.width <= LARGE_STRAIGHT.width || load.dimensions.width <= LARGE_STRAIGHT.length) {
        if (load.dimensions.length <= LARGE_STRAIGHT.width || load.dimensions.length <= LARGE_STRAIGHT.length) {
            if (load.dimensions.heigth <= LARGE_STRAIGHT.height || load.dimensions.height <= LARGE_STRAIGHT.height) {
                if (load.payload <= LARGE_STRAIGHT.payload) 
                {
                    const truck = await Truck.findOne({ status: 'IS', type: 'LARGE STRAIGHT' });
                    if (truck) {
                        if (truck.assigned_to) 
                        {
                            return truck;
                        }
                    }
                }
            }
        }
    }
}

const getLoads = async (userId, email) => {
    const user = await User.findOne({ email });
    if (user.role === 'DRIVER') {
        const loads = Load.find({ assigned_to: userId });
        return loads;
    } else {
        const loads = Load.find({ created_by: userId });
        return loads;
    }
}

const addLoad = async (userId, email, userLoad) => {
    const user = await User.findOne({ email });
    if (user.role !== 'SHIPPER') {
        throw new Error('Available only for shipper role');
    }
    const load = new Load({
        created_by: userId,
        name: userLoad.name,
        payload: userLoad.payload,
        pickup_address: userLoad.pickup_address,
        delivery_address: userLoad.delivery_address,
        dimensions: userLoad.dimensions
    });
    await load.save();
}

const getActiveLoads = async (userId, email) => {
    const user = await User.findOne({ email });
    if (user.role !== 'DRIVER') {
        throw new Error('Available only for driver role');
    }
    const load = Load.findOne({ assigned_to: userId, status: 'ASSIGNED' });
    return load;
}

const setLoadState = async (userId, email) => {
    const user = await User.findOne({ email });
    if (user.role !== 'DRIVER') {
        throw new Error('Available only for shipper role');
    }
    const load = await Load.findOne({ assigned_to: userId, status: 'ASSIGNED' });
    if (!load) {
        throw new Error('No active load');
    }
    console.log(load.state);
    if (load.state === 'En route to Pick Up') {
        await Load.findOneAndUpdate({ _id: load._id }, { state: 'Arrived to Pick Up' });
        return 'Arrived to Pick Up';
    } else if (load.state === 'Arrived to Pick Up') {
        await Load.findOneAndUpdate({ _id: load._id }, { state: 'En route to delivery' });
        return 'En route to delivery';
    } else if (load.state === 'En route to delivery') {
        await Load.findOneAndUpdate({ _id: load._id }, { state: 'Arrived to delivery', status: 'SHIPPED' });
        await Truck.findOneAndUpdate({ created_by: userId }, { state: 'IS' });
        return 'Arrived to delivery';
    }
}

const getLoad = async (userId, email, loadId) => {
    const user = await User.findOne({ email });
    if (user.role === 'DRIVER') {
        const load = Load.findOne({ assigned_to: userId, _id: loadId });
        return load;
    } else {
        const load = Load.findOne({ created_by: userId, _id: loadId });
        return load;
    }
}

const updateLoad = async (userId, email, loadId, updateLoad) => {
    const user = await User.findOne({ email });
    if (user.role !== 'SHIPPER') {
        throw new Error('Available only for shipper role');
    }
    const load = await Load.findOne({ created_by: userId, _id: loadId });
    if (!load) {
        throw new Error('Load not found');
    } else if (load.status !== 'NEW') {
        throw new Error('Available only for loads with status new');
    }
    await Load.findOneAndUpdate({ _id: load._id }, {
        name: updateLoad.name,
        payload: updateLoad.payload,
        pickup_address: updateLoad.pickup_address,
        delivery_address: updateLoad.delivery_address,
        dimensions: updateLoad.dimensions
    });
}

const deleteLoad = async (userId, email, loadId) => {
    const user = await User.findOne({ email });
    if (user.role !== 'SHIPPER') {
        throw new Error('Available only for shipper role');
    }
    const load = await Load.findOne({ created_by: userId, _id: loadId });
    if (!load) {
        throw new Error('Load not found');
    } else if (load.status !== 'NEW') {
        throw new Error('Available only for loads with status new');
    }
    await Load.findOneAndRemove({ created_by: userId, _id: loadId });
}

const postLoad = async (userId, email, loadId) => {
    const user = await User.findOne({ email });
    if (user.role !== 'SHIPPER') {
        throw new Error('Available only for shipper role');
    }
    const load = await Load.findOne({ created_by: userId, _id: loadId });
    if (!load) {
        throw new Error('Load not found');
    } else if (load.status !== 'NEW') {
        throw new Error('Available only for loads with status new');
    }
    await Load.findOneAndUpdate({ created_by: userId, _id: loadId }, { status: 'POSTED' });
    const truck = await findTruck(load);
    if (truck) {
        await Truck.findOneAndUpdate({ _id: truck._id }, { status: 'OL' });
        const logs = load.logs
        logs.push({
            message: `Load assigned to truck with id ${truck._id}`,
            time: Date.now()
        });
        await Load.findOneAndUpdate({ created_by: userId, _id: loadId }, {
            status: 'ASSIGNED',
            assigned_to: truck.created_by,
            state: 'En route to Pick Up',
            logs: logs
        });
        return true;
    } else {
        await Load.findOneAndUpdate({ created_by: userId, _id: loadId }, { status: 'NEW' });
        return false;
    }
}

const getShippingInfo = async (userId, email, loadId) => {
    const user = await User.findOne({ email });
    if (user.role === 'SHIPPER') {
        const load = await Load.findOne({ created_by: userId, _id: loadId });
        if (load.status === 'ASSIGNED') {
            const truck = await Truck.findOne({ assigned_to: load.assigned_to });
        console.log(truck)
        return { load, truck };
        }
        else {
            return { load };
        }
    }
}

module.exports = {
    getLoads,
    addLoad,
    getActiveLoads,
    setLoadState,
    getLoad,
    updateLoad,
    deleteLoad,
    postLoad,
    getShippingInfo
};