const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const apiRouter = require('./routes/apiRouter');
const port = 8080;
const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api', apiRouter);

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://user:123456qwerty@cluster0.9hlu5.mongodb.net/homework3?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });
        app.listen(port, console.log('Server is running'));
    } catch (error) {
        console.log(`Error on server setup: ${error}`);
    }
}

start();