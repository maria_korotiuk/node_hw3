const router = require('express').Router();
const loadsController = require('./../controllers/loadsController');

router.get('/', loadsController.getLoads);
router.post('/', loadsController.addLoad);
router.get('/active', loadsController.getActiveLoads);
router.patch('/active/state', loadsController.setLoadState);
router.get('/:id', loadsController.getLoad);
router.put('/:id', loadsController.updateLoad);
router.delete('/:id', loadsController.deleteLoad);
router.post('/:id/post', loadsController.postLoad);
router.get('/:id/shipping_info', loadsController.getShippingInfo);

module.exports = router;