const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
    created_by: {
        type: String,
        required: true
    },
    assigned_to: {
        type: String
    },
    status: {
        type: String,
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
        default: 'NEW',
        required: true
    },
    state: {
        type: String,
        enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery']
    },
    name: {
        type: String,
        required: true
    },
    payload: {
        type: Number,
        required: true
    },
    pickup_address: {
        type: String,
        required: true
    },
    delivery_address: {
        type: String,
        required: true
    },
    dimensions: {
        type: {
            width: {
                type: Number,
                required: true
            },
            length: {
                type: Number,
                required: true
            },
            height: {
                type: Number,
                required: true
            }
        },
        required: true
    },
    logs: {
        type: [{
            message: {
                type: String,
                required: true
            },
            time: {
                type: Date,
                required: true
            }
        }],
        required: true
    },
    created_date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = Load;