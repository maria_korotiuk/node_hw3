const loadsService = require('./../services/loadsService');

const getLoads = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const loads = await loadsService.getLoads(userId, email);
        res.status(200).send({
            loads
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const addLoad = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const load = req.body;
        await loadsService.addLoad(userId, email, load);
        res.status(200).send({
            "message": "Load created successfully"
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const getActiveLoads = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const load = await loadsService.getActiveLoads(userId, email);
        res.status(200).send({
            load
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const setLoadState = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const state = await loadsService.setLoadState(userId, email);
        const message = `Load state changed to ${state}`;
        res.status(200).send({
            "message": message
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const getLoad = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const loadId = req.params.id;
        const load = await loadsService.getLoad(userId, email, loadId);
        res.status(200).send({
            load
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const updateLoad = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const loadId = req.params.id;
        const load = req.body;
        await loadsService.updateLoad(userId, email, loadId, load);
        res.status(200).send({
            "message": "Load details changed successfully"
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const deleteLoad = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const loadId = req.params.id;
        await loadsService.deleteLoad(userId, email, loadId);
        res.status(200).send({
            "message": "Load deleted successfully"
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const postLoad = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const loadId = req.params.id;
        const driverFound = await loadsService.postLoad(userId, email, loadId);
        res.status(200).send({
            "message": "Load posted successfully",
            "driver_found": driverFound
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const getShippingInfo = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const loadId = req.params.id;
        const {
            load,
            truck
        } = await loadsService.getShippingInfo(userId, email, loadId);
        if (!truck) {
            res.status(200).send({
                load
            });
        } else {
            res.status(200).send({
                load,
                truck
            });
        }
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

module.exports = {
    getLoads,
    addLoad,
    getActiveLoads,
    setLoadState,
    getLoad,
    updateLoad,
    deleteLoad,
    postLoad,
    getShippingInfo
};