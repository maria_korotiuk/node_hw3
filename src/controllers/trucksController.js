const trucksService = require('./../services/trucksService');

const getTrucks = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const trucks = await trucksService.getTrucks(userId, email);
        res.status(200).send({
            trucks
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const addTruck = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const type = req.body.type;
        await trucksService.addTruck(userId, email, type);
        res.status(200).send({
            "message": "Truck created successfully"
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const getTruck = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const id = req.params.id;
        const truck = await trucksService.getTruck(userId, email, id);
        res.status(200).send({
            "truck": {
                "_id": truck._id,
                "created_by": truck.created_by,
                "assigned_to": truck.assigned_to,
                "type": truck.type,
                "status": truck.status,
                "created_date": truck.created_date
            }
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const updateTruck = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const id = req.params.id;
        const type = req.body.type;
        await trucksService.updateTruck(userId, email, id, type);
        res.status(200).send({
            "message": "Truck details changed successfully"
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const deleteTruck = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const id = req.params.id;
        await trucksService.deleteTruck(userId, email, id);
        res.status(200).send({
            "message": "Truck deleted successfully"
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const assignTruck = async (req, res) => {
    try {
        const {
            userId,
            email
        } = req.user;
        const id = req.params.id;
        await trucksService.assignTruck(userId, email, id);
        res.status(200).send({
            "message": "Truck assigned successfully"
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

module.exports = {
    getTrucks,
    addTruck,
    getTruck,
    updateTruck,
    deleteTruck,
    assignTruck
}