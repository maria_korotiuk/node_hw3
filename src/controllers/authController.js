const authService = require('./../services/authService');

const register = async (req, res) => {
    try {
        const {
            email,
            password,
            role
        } = req.body;
        await authService.registration(email, password, role);
        res.status(200).send({
            "message": "Profile created successfully"
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

const login = async (req, res) => {
    try {
        const {
            email,
            password
        } = req.body;
        const jwt_token = await authService.loginUser(email, password);
        res.status(200).send({
            jwt_token
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

module.exports = {
    register,
    login
}