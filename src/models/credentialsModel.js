const mongoose = require('mongoose');

const Credentials = mongoose.model('Credentials', {
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
});

module.exports = Credentials;