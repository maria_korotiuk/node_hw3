const mongoose = require('mongoose');

const RegistrationCredentials = mongoose.model('RegistrationCredentials', {
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ['SHIPPER', 'DRIVER'],
        required: true
  }
});

module.exports = RegistrationCredentials;