const mongoose = require('mongoose');

const User = mongoose.model('User', {
    role: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    created_date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = User;