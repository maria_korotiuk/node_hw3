const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
    created_by: {
        type: String,
        required: true
    },
    assigned_to: {
        type: String
    },
    type: {
        type: String,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
        required: true
    },
    status: {
        type: String,
        enum: ['OL', 'IS'],
        default: 'IS',
        required: true
    },
    created_date: {
        type: Date,
        default: Date.now(),
        required: true
    }
});

module.exports = Truck;