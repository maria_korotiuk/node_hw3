const router = require('express').Router();
const usersRouter = require('./usersRouter');
const trucksRouter = require('./trucksRouter');
const loadsRouter = require('./loadsRouter');
const authRouter = require('./authRouter');
const { authMiddleware } = require('./../middelwares/authMiddelware');

router.use('/users', authMiddleware, usersRouter);
router.use('/trucks', authMiddleware, trucksRouter);
router.use('/loads', authMiddleware, loadsRouter);
router.use('/auth', authRouter);

module.exports = router;