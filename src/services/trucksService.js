const Truck = require('./../models/truckModel');
const User = require('./../models/userModel');

const getTrucks = async (userId, email) => {
    const user = await User.findOne({ email });
    if (user.role !== 'DRIVER') {
        throw new Error('Available only for driver role');
    }
    const trucks = await Truck.find({ created_by: userId });
    return trucks;
}

const addTruck = async (userId, email, type) => {
    const user = await User.findOne({ email });
    if (user.role !== 'DRIVER') {
        throw new Error('Available only for driver role');
    }
    const truck = new Truck({
        created_by: userId,
        type
    });
    await truck.save();
}

const getTruck = async (userId, email, id) => {
    const user = await User.findOne({ email });
    if (user.role !== 'DRIVER') {
        throw new Error('Available only for driver role');
    }
    const truck = await Truck.findOne({ _id: id, created_by: userId });
    return truck;
}

const updateTruck = async (userId, email, id, type) => {
    const user = await User.findOne({ email });
    if (user.role !== 'DRIVER') {
        throw new Error('Available only for driver role');
    }
    const truck = await Truck.findOne({ _id: id, created_by: userId });
    console.log(truck.assigned_to);
    if (truck.assigned_to) {
        throw new Error('Updating is unavailable for assigned truck');
    }
    await Truck.findOneAndUpdate({ _id: id, created_by: userId }, { type });
}

const deleteTruck = async (userId, email, id) => {
    const user = await User.findOne({ email });
    if (user.role !== 'DRIVER') {
        throw new Error('Available only for driver role');
    }
    const truck = await Truck.findOne({ _id: id, created_by: userId });
    if (truck.assigned_to) {
        throw new Error('Deleting is unavailable for assigned truck');
    }
    await Truck.findOneAndRemove({ _id: id, created_by: userId });
}

const assignTruck = async (userId, email, id) => {
    const user = await User.findOne({ email });
    if (user.role !== 'DRIVER') {
        throw new Error('Available only for driver role');
    }
    const truck = await Truck.findOne({ _id: id, created_by: userId });
    if (truck.assigned_to) {
        throw new Error('Truck has been already assigned');
    }
    await Truck.findOneAndUpdate({ _id: id, created_by: userId }, { assigned_to: userId });
}

module.exports = {
    getTrucks,
    addTruck,
    getTruck,
    updateTruck,
    deleteTruck,
    assignTruck
};