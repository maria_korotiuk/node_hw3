const usersService = require('./../services/usersService');

const getUser = async (req, res) => {
    try {
        const email = req.user.email;
        const user = await usersService.getUser(email);
        res.status(200).send({
            "user": {
                "_id": user._id,
                "role": user.role,
                "email": user.email,
                "created_date": user.created_date
            }
        });
    } catch (error) {
        res.status(400).send({
           "message": error.message 
        });
    }
}

const deleteUser = async (req, res) => {
    try {
        const email = req.user.email;
        await usersService.deleteUser(email);
        res.status(200).send({
            "message": "Profile deleted successfully"
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message 
         });
    }
}

const changePassword = async (req, res) => {
    try {
        const email = req.user.email;
        const { 
            oldPassword,
            newPassword
        } = req.body;
        await usersService.changePassword(email, oldPassword, newPassword);
        res.status(200).send({
            "message": "Password changed successfully"
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message 
         });
    }
}

module.exports = {
    getUser,
    deleteUser,
    changePassword
}