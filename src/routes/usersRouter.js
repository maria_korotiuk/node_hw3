const router = require('express').Router();
const usersController = require('./../controllers/usersController');

router.get('/me', usersController.getUser);
router.delete('/me', usersController.deleteUser);
router.patch('/me/password', usersController.changePassword);

module.exports = router;